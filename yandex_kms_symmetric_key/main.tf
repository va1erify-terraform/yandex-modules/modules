# Ключ Yandex Key Management Service для шифрования важной информации,такой как пароли, OAuth-токены и SSH-ключи.
resource "yandex_kms_symmetric_key" "kms_key" {
  name              = var.name
  default_algorithm = var.default_algorithm
  rotation_period   = var.rotation_period
}
output "id" {
  value = yandex_kms_symmetric_key.kms_key.id
}

output "name" {
  value = yandex_kms_symmetric_key.kms_key.name
}

output "rotation_period" {
  value = yandex_kms_symmetric_key.kms_key.rotation_period
}

output "default_algorithm" {
  value = yandex_kms_symmetric_key.kms_key.default_algorithm
}


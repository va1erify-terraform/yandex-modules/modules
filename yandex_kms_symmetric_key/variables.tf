variable "name" {
  default = "default_kms_key"
}
variable "default_algorithm" {
  default = "AES_128"
}
variable "rotation_period" {
  default = "8760h"
  description = "1 year = 8760h"
}
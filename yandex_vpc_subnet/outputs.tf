output "id" {
  value = yandex_vpc_subnet.subnetwork.id
}

output "network_id" {
  value = yandex_vpc_subnet.subnetwork.network_id
}

output "zone" {
  value = yandex_vpc_subnet.subnetwork.zone
}

output "v4_cidr_blocks" {
  value = yandex_vpc_subnet.subnetwork.v4_cidr_blocks
}
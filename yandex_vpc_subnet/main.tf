resource "yandex_vpc_subnet" "subnetwork" {
  name           = var.name
  network_id     = var.id
  v4_cidr_blocks = [var.cidr_v4]
  zone           = var.zone
}
output "external_ipv4_address_name" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.name
}

output "external_ipv4_address_description" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.description
}

output "external_ipv4_address_folder_id" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.folder_id
}

output "external_ipv4_address_labels" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.labels
}

output "external_ipv4_address_deletion_protection" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.deletion_protection
}

output "external_ipv4_address_zone_id" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.external_ipv4_address[0].zone_id
}

output "external_ipv4_address" {
  value = yandex_vpc_address.yandex_vpc_external_ipv4_address.external_ipv4_address[0].address
}

variable "name" {
  default = "ipv4_address_default_name"
}

variable "description" {
  default = "ipv4_address_default_description"
}

variable "folder_id" {
  default = "ipv4_address_default_folder_id"
}

variable "labels" {
  default = {
    label1 = "exampleLabel"
  }
}

variable "deletion_protection" {
  default = "false"
}

variable "zone_id" {
  default = "ipv4_address_default_zone_id"
}
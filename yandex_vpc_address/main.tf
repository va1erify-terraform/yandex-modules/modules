resource "yandex_vpc_address" "yandex_vpc_external_ipv4_address" {
  name                = var.name
  description         = var.description
  folder_id           = var.folder_id
  labels              = var.labels
  deletion_protection = var.deletion_protection

  external_ipv4_address {
    zone_id                  = var.zone_id
  }
}
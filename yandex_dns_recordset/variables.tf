variable "zone_id" {
  default = "srv.example.com."
}
variable "name" {
  default = "srv.example.com."
}
variable "type" {
  default = "A"
}
variable "ttl" {
  default = 200
}
variable "data" {
  type = list(string)
  default = ["10.1.0.1", "10.1.0.2", "10.1.0.3"]
}
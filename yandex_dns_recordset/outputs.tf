output "name" {
  value = yandex_dns_recordset.rs1.name
}

output "id" {
  value = yandex_dns_recordset.rs1.id
}

output "zone_id" {
  value = yandex_dns_recordset.rs1.zone_id
}

output "type" {
  value = yandex_dns_recordset.rs1.type
}

output "ttl" {
  value = yandex_dns_recordset.rs1.ttl
}

output "data" {
  value = yandex_dns_recordset.rs1.data
}

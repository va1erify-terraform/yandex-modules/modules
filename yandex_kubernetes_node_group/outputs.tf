output "name" {
  value = yandex_kubernetes_node_group.k8s_node_group.name
}

output "id" {
  value = yandex_kubernetes_node_group.k8s_node_group.id
}


output "cluster_id" {
  value = yandex_kubernetes_node_group.k8s_node_group.cluster_id
}

output "instance_group_id" {
  value = yandex_kubernetes_node_group.k8s_node_group.instance_group_id
}

output "instance_template" {
  value = yandex_kubernetes_node_group.k8s_node_group.instance_template
}

output "labels" {
  value = yandex_kubernetes_node_group.k8s_node_group.labels
}

output "maintenance_policy" {
  value = yandex_kubernetes_node_group.k8s_node_group.maintenance_policy
}

output "created_at" {
  value = yandex_kubernetes_node_group.k8s_node_group.created_at
}

output "version" {
  value = yandex_kubernetes_node_group.k8s_node_group.version
}

output "status" {
  value = yandex_kubernetes_node_group.k8s_node_group.status
}

output "scale_policy" {
  value = yandex_kubernetes_node_group.k8s_node_group.scale_policy
}

variable "maintenance_policy_auto_upgrade" {
  default = "true"
}
variable "maintenance_policy_auto_repair" {
  default = "true"
}
variable "maintenance_window_day" {
  default = "monday"
}
variable "maintenance_window_start_time" {
  default = "15:00"
}
variable "maintenance_window_duration" {
  default = "3h"
}

variable "cluster_id" {}

variable "name" {
  default = "default-node"
}
variable "platform_id" {
  default = "standard-v3"
}
variable "nat" {
  default = "true"
}
variable "memory" {
  default = "4"
}
variable "cores" {
  default = "2"
}
variable "core_fraction" {
  default = "20"
}
variable "boot_disk_type" {
  default = "network-hdd"
}
variable "boot_disk_size" {
  default = "30"
}
variable "preemptible" {
  default = "false"
}
variable "container_runtime_type" {
  default = "containerd"
}
variable "scale_policy_min" {
  default = "1"
}
variable "scale_policy_max" {
  default = "2"
}
variable "scale_policy_initial" {
  default = "1"
}
variable "zone" {
  default = "ru-central1-a"
}

variable "security_group_ids" {
  type    = list(string)
  default = ["yandex_vpc_security_group.vpc_security_group.id"]
}

variable "subnet_ids" {
  type    = list(string)
  default = ["id1", "id2"]
}

variable "labels" {
  type    = map(string)
  default = {
    "key" = "value"
  }
}
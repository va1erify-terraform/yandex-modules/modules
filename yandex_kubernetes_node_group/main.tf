resource "yandex_kubernetes_node_group" "k8s_node_group" {
  cluster_id = var.cluster_id
  name       = var.name

  labels = var.labels

  instance_template {
    platform_id = var.platform_id
    network_interface {
      nat                = var.nat
      subnet_ids         = var.subnet_ids
      security_group_ids = var.security_group_ids
    }

    resources {
      memory        = var.memory
      cores         = var.cores
      core_fraction = var.core_fraction
    }

    boot_disk {
      type = var.boot_disk_type
      size = var.boot_disk_size
    }

    scheduling_policy {
      preemptible = var.preemptible
    }

    container_runtime {
      type = var.container_runtime_type
    }
  }

  scale_policy {
    auto_scale {
      min     = var.scale_policy_min
      max     = var.scale_policy_max
      initial = var.scale_policy_initial
    }
  }

  allocation_policy {
    location {
      zone = var.zone
    }
  }

  maintenance_policy {
    auto_upgrade = var.maintenance_policy_auto_upgrade
    auto_repair  = var.maintenance_policy_auto_repair

    maintenance_window {
      day        = var.maintenance_window_day
      start_time = var.maintenance_window_start_time
      duration   = var.maintenance_window_duration
    }
  }
}

variable "folder_id" {}

variable "zone" {
  default = "ru-central1-a"
}

variable "s3_bucket_name" {
  default = "val1erify-s3-bucket-test"
}

variable "s3_service_account_name" {
  default = "s3-service-account-test"
}

variable "s3_service_account_role" {
  default = "storage.editor"
}

variable "s3_storage_bucket_read" {
  default = false
}

variable "s3_storage_bucket_list" {
  default = false
}

variable "s3_storage_bucket_config_read" {
  default = false
}

variable "s3_storage_bucket_max_size" {
  default = 104857601
}

variable "s3_storage_bucket_force_destroy" {
  default = true
}
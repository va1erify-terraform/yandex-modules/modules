output "name" {
  value = var.s3_bucket_name
}

output "s3_access_key" {
  sensitive = true
  value     = yandex_iam_service_account_static_access_key.s3_service_account_static_key.access_key
}

output "s3_secret_key" {
  sensitive = true
  value     = yandex_iam_service_account_static_access_key.s3_service_account_static_key.secret_key
}

output "region" {
  value = var.zone
  sensitive = false
}
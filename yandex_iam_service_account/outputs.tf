# A service account can be imported using the id of the resource, e.g.
# terraform import yandex_iam_service_account.sa account_id

output "id" {
  value = yandex_iam_service_account.sa.id
}

output "name" {
  value = yandex_iam_service_account.sa.name
}

output "folder_id" {
  value = yandex_iam_service_account.sa.folder_id
}

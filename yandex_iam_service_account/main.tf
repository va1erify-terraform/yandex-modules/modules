resource "yandex_iam_service_account" "sa" {
  name        = var.name
  description = var.description
  folder_id   = var.folder_id
}
variable "network_name" {
  default = "default-network-name"
}

variable "vpc_security_group_name" {
  default = "default-security-group"
}

variable "network_id" {
  default = "default-network-id"
}

variable "subnetwork_v4_cidr_blocks" {
  default = "10.10.10.0/24"
}
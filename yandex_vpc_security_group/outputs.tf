output "id" {
  value = yandex_vpc_security_group.vpc_security_group.id
}

output "folder_id" {
  value = yandex_vpc_security_group.vpc_security_group.folder_id
}

output "network_id" {
  value = yandex_vpc_security_group.vpc_security_group.network_id
}

output "name" {
  value = yandex_vpc_security_group.vpc_security_group.name
}

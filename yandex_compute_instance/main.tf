resource "yandex_compute_instance" "vm" {
  name        = var.name
  platform_id = var.platform
  hostname    = var.hostname
  zone        = var.zone

  scheduling_policy {
    preemptible = var.preemptible
  }

  resources {
    cores         = var.cpu
    memory        = var.ram
    core_fraction = var.core_fraction
  }

  boot_disk {
    initialize_params {
      image_id   = var.boot_disk_image_id
      size       = var.boot_disk_size
      type       = var.boot_disk_type
      block_size = var.boot_disk_block_size
    }
  }

  dynamic "secondary_disk" {
    for_each = var.secondary_disk_ids
    content {
      disk_id = secondary_disk.value
    }
  }

  network_interface {
    subnet_id  = var.subnetwork_id
    nat        = var.nat
    ip_address = var.internal_ip_address
  }

  metadata = {
    user-data = file(var.path_to_cloud_config)
  }
}
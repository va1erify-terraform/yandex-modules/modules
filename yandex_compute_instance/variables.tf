variable "name" {
  description = "Name VM in YC"
  default = "default-name"
}

variable "nat_ip_address" {
  type    = string
  default = null
  description = "Опциональный статический публичный IP для NAT. Если не задано — IP назначается автоматически."
}

variable "zone" {
  type    = string
  default = null
  description = "Зона для создания виртуальных машин"
}

variable "internal_ip_address" {
  description = "internal ip address"
  default = "192.168.0.3"
}

variable "hostname" {
  description = "Vm hostname"
  default = "hostname"
}

variable "platform" {
  default = "standard-v2"
  description = "Platform YCloud"
}

variable "ram" {
  default = 0.5
  description = "Count RAM in GB"
}

variable "cpu" {
  default = 2
  description = "Count CPU"
}
variable "subnetwork_id" {}

variable "core_fraction" {
  default = 5
}

variable "boot_disk_image_id" {
  default = "fd8t849k1aoosejtcicj"
}
variable "boot_disk_size" {
  default = 5
}

variable "preemptible" {
  default = true
}

variable "boot_disk_type" {
  default = "network-hdd"
}

variable "boot_disk_block_size" {
  default = 4096
}

variable "nat" {
  default = true
}

variable "path_to_cloud_config" {
  default = "./cloud-config"
}

variable "secondary_disk_ids" {
  type = list(string)
  default = []
}
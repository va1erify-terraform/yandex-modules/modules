variable "folder_id" {
  default = ""
}

variable "name" {
  default = "my-private-zone"
}

variable "description" {
  default = "desc"
}

variable "zone" {
  default = "example.com."
}

variable "public" {
  default = "false"
}

variable "private_networks" {
  type = set(string)
  default = ["yandex_vpc_network.foo.id"]
}

variable "deletion_protection" {
  default = "true"
}

variable "labels" {
  type = map(string)
  default = {
    label1 = "label-1-value"
    label2 = "label-2-value"
    label3 = "label-3-value"
  }
}
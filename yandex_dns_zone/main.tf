resource "yandex_dns_zone" "zone1" {
  folder_id = var.folder_id
  name        = var.name
  description = var.description

  labels = var.labels

  zone             = var.zone
  public           = var.public
  private_networks = var.private_networks

  deletion_protection = var.deletion_protection
}
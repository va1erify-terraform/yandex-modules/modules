output "name" {
  value = yandex_dns_zone.zone1.name
}

output "description" {
  value = yandex_dns_zone.zone1.description
}

output "zone" {
  value = yandex_dns_zone.zone1.zone
}

output "private_networks" {
  value = yandex_dns_zone.zone1.private_networks
}

output "deletion_protection" {
  value = yandex_dns_zone.zone1.deletion_protection
}

output "public" {
  value = yandex_dns_zone.zone1.public
}

output "labels" {
  value = yandex_dns_zone.zone1.labels
}

output "id" {
  value = yandex_dns_zone.zone1.id
}

resource "yandex_dns_zone_iam_binding" "viewer" {
  dns_zone_id = var.dns_zone_id
  role        = var.role
  members     = var.members
}
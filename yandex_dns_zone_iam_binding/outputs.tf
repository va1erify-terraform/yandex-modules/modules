output "id" {
  value = yandex_dns_zone_iam_binding.viewer.id
}

output "role" {
  value = yandex_dns_zone_iam_binding.viewer.role
}

output "members" {
  value = yandex_dns_zone_iam_binding.viewer.members
}

output "dns_zone_id" {
  value = yandex_dns_zone_iam_binding.viewer.dns_zone_id
}
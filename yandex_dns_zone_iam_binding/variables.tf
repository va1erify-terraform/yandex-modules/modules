variable "dns_zone_id" {
  default = ""
}

variable "role" {
  default = "dns.viewer"
}

# members - (Required) Identities that will be granted the privilege in role.
# Each entry can have one of the following values:
#
# userAccount:{user_id}: A unique user ID that represents a specific Yandex account.
# serviceAccount:{service_account_id}: A unique service account ID.
# federatedUser:{federated_user_id}:: A unique saml federation user account ID.
# group:{group_id}: A unique group ID.
# system:{allUsers|allAuthenticatedUsers}: see system groups
variable "members" {
  type = list(string)
  default = ["userAccount:foo_user_id"]
}
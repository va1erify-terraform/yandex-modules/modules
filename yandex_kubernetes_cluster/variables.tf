variable "subnetwork_zone" {}
variable "subnetwork_id" {}
variable "network_id" {}

variable "cluster_name" {
  default = "k8s-default-cluster"
}

variable "cluster_public_ip" {
  default = "true"
}

variable "k8_node_service_account" {}
variable "k8_service_account" {}

variable "master_version" {
  default = "1.17"
}

variable "description" {
  default = "Description"
}

variable "vpc_security_group_id" {
  type = list(string)
  default = ["00000"]
}

variable "maintenance_policy_auto_upgrade" {
  default = "true"
}
variable "maintenance_window_start_time" {
  default = "02:00"
}
variable "maintenance_window_duration" {
  default = "1h"
}
variable "master_logging_enabled" {
  default = "true"
}
variable "master_logging_kube_apiserver_enabled" {
  default = "true"
}
variable "master_logging_cluster_autoscaler_enabled" {
  default = "true"
}
variable "master_logging_events_enabled" {
  default = "true"
}
variable "master_logging_audit_enabled" {
  default = "true"
}
variable "master_logging_log_group_id" {
  default = "true"
}
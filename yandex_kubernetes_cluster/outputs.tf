output "id" {
  value = yandex_kubernetes_cluster.k8_cluster.id
}

output "folder_id" {
  value = yandex_kubernetes_cluster.k8_cluster.folder_id
}

output "name" {
  value = yandex_kubernetes_cluster.k8_cluster.name
}

output "network_id" {
  value = yandex_kubernetes_cluster.k8_cluster.network_id
}

output "cluster_ipv4_range" {
  value = yandex_kubernetes_cluster.k8_cluster.cluster_ipv4_range
}
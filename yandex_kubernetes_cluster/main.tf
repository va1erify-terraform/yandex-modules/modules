resource "yandex_kubernetes_cluster" "k8_cluster" {
  name        = var.cluster_name
  network_id  = var.network_id
  description = var.description

  service_account_id      = var.k8_service_account.id
  node_service_account_id = var.k8_node_service_account.id


  master {
    version = var.master_version
    zonal {
      zone      = var.subnetwork_zone
      subnet_id = var.subnetwork_id
    }
    public_ip          = var.cluster_public_ip
    security_group_ids = var.vpc_security_group_id

    maintenance_policy {
      auto_upgrade = var.maintenance_policy_auto_upgrade

      maintenance_window {
        start_time = var.maintenance_window_start_time
        duration   = var.maintenance_window_duration
      }
    }

    # FIXME
    #    master_logging {
    #      enabled                    = var.master_logging_enabled
    #      log_group_id               = var.master_logging_log_group_id
    #      kube_apiserver_enabled     = var.master_logging_kube_apiserver_enabled
    #      cluster_autoscaler_enabled = var.master_logging_cluster_autoscaler_enabled
    #      events_enabled             = var.master_logging_events_enabled
    #      audit_enabled              = var.master_logging_audit_enabled
    #    }
  }

  # FIXME
  #  kms_provider {
  #    key_id = yandex_kms_symmetric_key.kms_key.id
  #  }

}
# Allows creation and management of a single binding within IAM policy for an existing Yandex Resource Manager folder.

# ~> Note: This resource must not be used in conjunction with
# yandex_resourcemanager_folder_iam_policy or they will conflict over what your policy should be.

# ~> Note: When you delete yandex_resourcemanager_folder_iam_binding resource,
# the roles can be deleted from other users within the folder as well. Be careful!


resource "yandex_resourcemanager_folder_iam_binding" "admin" {
  folder_id = var.folder_id

  role = var.role

  members = var.members_list
}




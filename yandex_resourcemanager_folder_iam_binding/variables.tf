variable "folder_id" {}

variable "role" {
  default = "editor"
}

variable "members_list" {
  type    = list(string)
  default = ["userAccount:some_user_id", "userAccount2:some_user_i2"]
}

# members - (Required) An array of identities that will be granted the privilege that is specified in the role field.
# Each entry can have one of the following values:
# userAccount:{user_id}: An email address that represents a specific Yandex account. For example, ivan@yandex.ru or joe@example.com.
# serviceAccount:{service_account_id}: A unique service account ID.
# federatedUser:{federated_user_id}:: A unique saml federation user account ID.
# group:{group_id}: A unique group ID.
# system:{allUsers|allAuthenticatedUsers}: see system groups
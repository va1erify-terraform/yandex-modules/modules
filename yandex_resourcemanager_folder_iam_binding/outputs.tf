#IAM binding imports use space-delimited identifiers; first the resource in question and then the role.
#These bindings can be imported using the folder_id and role, e.g.
#$ terraform import yandex_resourcemanager_folder_iam_binding.viewer "folder_id viewer"

output "folder_id" {
  value = yandex_resourcemanager_folder_iam_binding.admin.folder_id
}

output "members" {
  value = yandex_resourcemanager_folder_iam_binding.admin.members
}

output "id" {
  value = yandex_resourcemanager_folder_iam_binding.admin.id
}

output "role" {
  value = yandex_resourcemanager_folder_iam_binding.admin.role
}
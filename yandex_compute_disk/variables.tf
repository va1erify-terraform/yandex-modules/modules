variable "name" {
  default = "second_disk"
}
variable "size" {
  default = "10"
}
variable "type" {
  default = "network-ssd"
}
variable "zone" {
  default = "ru-central1-a"
}
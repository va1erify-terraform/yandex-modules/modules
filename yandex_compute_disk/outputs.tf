output "name" {
  value = yandex_compute_disk.disk.name
}

output "size" {
  value = yandex_compute_disk.disk.size
}

output "type" {
  value = yandex_compute_disk.disk.type
}

output "zone" {
  value = yandex_compute_disk.disk.zone
}

output "id" {
  value = yandex_compute_disk.disk.id
}
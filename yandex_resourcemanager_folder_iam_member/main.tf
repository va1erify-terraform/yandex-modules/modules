# Allows creation and management of a single member for a single binding within
# the IAM policy for an existing Yandex Resource Manager folder.
#
# ~> Note: This resource must not be used in conjunction with
# yandex_resourcemanager_folder_iam_policy or they will conflict over what your policy should be. Similarly, roles controlled by yandex_resourcemanager_folder_iam_binding
# should not be assigned using yandex_resourcemanager_folder_iam_member.

resource "yandex_resourcemanager_folder_iam_member" "admin" {
  folder_id = var.folder_id

  role   = var.role

  member =  var.members_list
}

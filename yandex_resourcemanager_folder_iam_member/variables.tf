variable "role" {
  default = "editor"
}
variable "folder_id" {}

# member - (Required) The identity that will be granted the privilege that is specified in the role field.
# This field can have one of the following values:
#
# userAccount:{user_id}: A unique user ID that represents a specific Yandex account.
# serviceAccount:{service_account_id}: A unique service account ID.
# federatedUser:{federated_user_id}:: A unique saml federation user account ID.
# group:{group_id}: A unique group ID.
# system:{allUsers|allAuthenticatedUsers}: see system groups

variable "members_list" {
  type    = string
  default = "userAccount:some_user_id"
}
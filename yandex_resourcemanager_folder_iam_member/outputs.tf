# IAM member imports use space-delimited identifiers; the resource in question, the role, and the account.
# This member resource can be imported using the folder id, role, and account, e.g.
# terraform import yandex_resourcemanager_folder_iam_member.my_project "folder_id viewer foo@example.com"

output "role" {
  value = yandex_resourcemanager_folder_iam_member.admin.role
}

output "member" {
  value = yandex_resourcemanager_folder_iam_member.admin.member
}

output "folder_id" {
  value = yandex_resourcemanager_folder_iam_member.admin.folder_id
}

output "id" {
  value = yandex_resourcemanager_folder_iam_member.admin.id
}
